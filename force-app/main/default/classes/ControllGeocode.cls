public class ControllGeocode {
    public static void controllInvoke(List<Account> accounts){ 
        for(Account account: accounts){
            if(System.isFuture()){
                return;            
            }else{
                GeocodeAddress.geocodeAddres(account.Id);
            }         
        }
    }
}