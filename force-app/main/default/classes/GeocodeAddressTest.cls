@isTest
public class GeocodeAddressTest {
	@isTest 
    public static void geocodeAddresTest(){
        Account ac = new Account(Name = 'Denis', BillingCity = 'Minsk');
        insert ac;
        Test.setMock(HttpCalloutMock.class, new GeocodeAddressMock());
        Test.startTest();
		GeocodeAddress.geocodeAddres(ac.Id);        
        Test.stopTest();
        Account acc = new Account();
        acc = [SELECT BillingLongitude,BillingLatitude from Account where Id =: ac.Id limit 1];
        System.assertNotEquals(null,acc.BillingLatitude);
        System.assertNotEquals(null,acc.BillingLongitude);
    }
}