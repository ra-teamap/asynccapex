public class GeocodeAddress {
    public static final String GET = 'GET';
    @future (callout=true)
    static public void geocodeAddres(Id accountId)
    { 
        final String site = 'Google';
        EndPoint__mdt GeocodeApi;
        GeocodeApi = [SELECT Key__c, EndPoint__c from EndPoint__mdt where Site__c  =:Site];
        System.debug(GeocodeApi);
       	String endPoint = GeocodeApi.EndPoint__c;
        String geocodingKey = GeocodeApi.Key__c;
        Account geoAccount = [SELECT Name, BillingStreet, BillingCity, BillingState, 
                              BillingCountry, BillingLatitude, BillingLongitude FROM Account WHERE
                              Id = :accountId];
        String geoAddress = '';
        if(String.isNotBlank(geoAccount.BillingCity)){
            system.debug('city');
            geoAddress+= geoAccount.BillingCity+ ', ';
        }       
        if(String.isNotBlank(geoAccount.BillingState)){
            geoAddress+= geoAccount.BillingState+ ', ';
        }       
        if(String.isNotBlank(geoAccount.BillingCountry)){
            geoAddress+= geoAccount.BillingCountry+ ', ';
        }     
        
        geoAddress= EncodingUtil.urlEncode(geoAddress, 'UTF-8');
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint + '?address='+geoAddress +'&key='+ geocodingKey+'&sensor=false');
        request.setMethod(GET);
        request.setTimeout(60000);
        try {
            HttpResponse response = http.send(request);
            GeolocationWrapper wrap = (GeolocationWrapper) JSON.deserialize(response.getBody(),GeolocationWrapper.class);
            Double latitude =  wrap.results[0].geometry.location.lat;
            Double longitude = wrap.results[0].geometry.location.lng;
            system.debug(wrap.results[0].geometry.location.lat);
            system.debug(wrap.results[0].geometry.location.lng);
            if(latitude != null) {
                geoAccount.BillingLatitude= latitude;
                geoAccount.BillingLongitude= longitude;
            }
        } catch(Exception e) {
                System.debug(LoggingLevel.ERROR,
                             'Error Geocoding Address - ' + e.getMessage());
            }
        update geoAccount;
    }
}