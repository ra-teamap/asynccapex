trigger AccountTrigger on Account (after insert, after update) { 
    if(Trigger.isUpdate || Trigger.isInsert) {
        ControllGeocode.controllInvoke(Trigger.new);
    }
}